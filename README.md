This is a repository containing the pin configuration of OBC and ADCS MCUs.

To open the project follow these steps:
- Clone the repository.
- Go to the `Harmony3 folder` -> `mhc`, if you are on Linux, it's porbably inside your Home directory.
- Open the terminal there and run `sh runmhc.sh`,if might need Java 8 to work.
- When the MPLAB Harmony Configurator opens, go to `File`->`Open Configuration`, then locate and open the .mch file of this repository and press `Open`.
- After the project loads, go to `Tools`->`Pin Configuration`.
You are good to go!
